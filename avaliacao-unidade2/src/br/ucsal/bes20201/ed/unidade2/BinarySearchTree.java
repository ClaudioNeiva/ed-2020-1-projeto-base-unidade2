package br.ucsal.bes20201.ed.unidade2;

public class BinarySearchTree<T extends Comparable<T>> {

	protected Node<T> root;

	public void add(T element) {
		root = add(element, root);
	}

	public Node<T> search(T element) {
		return search(element, root);
	}

	private Node<T> add(T element, Node<T> node) {
		if (node == null) {
			node = new Node<T>(null, element, null);
		} else {
			int compare = element.compareTo(node.element);
			if (compare < 0) {
				node.left = add(element, node.left);
			} else if (compare > 0) {
				node.right = add(element, node.right);
			}
		}
		return node;
	}

	private Node<T> search(T element, Node<T> node) {
		if (node == null) {
			return null;
		}
		int compare = element.compareTo(node.element);
		if (compare < 0) {
			return search(element, node.left);
		} else if (compare > 0) {
			return search(element, node.right);
		}
		return node;
	}

}
