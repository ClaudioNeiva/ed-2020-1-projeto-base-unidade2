package br.ucsal.bes20201.ed.unidade2;

public class Node<T extends Comparable<T>> {

	T element;

	Node<T> left;

	Node<T> right;

	public Node(Node<T> left, T element, Node<T> right) {
		this.element = element;
		this.left = left;
		this.right = right;
	}

}
